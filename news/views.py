from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
import json
from datetime import datetime
from news.models import Story


# Create your views here.

@csrf_exempt
def HandleLogin(request):
    if request.method == 'POST':
        un = request.POST['username']
        pw = request.POST['password']
        user = authenticate(request, username=un, password=pw)
        if user is not None:
            if user.is_active:
                login(request, user)
                if user.is_authenticated:
                    print(user.username + ' is logged in')
                    request.session['username'] = un
                    result = "welcome " + un
                    payload = result
                    http_response = HttpResponse(payload)
                    http_response['Content-Type'] = 'text/plain'
                    http_response.status_code = 200
                    http_response.reason_phrase = "OK"
                    return http_response
            else:
                result = "Disabled account"
                payload = result
                http_response = HttpResponse(payload)
                http_response['Content-Type'] = 'text/plain'
                http_response.status_code = 403
                http_response.reason_phrase = "Disabled Account"
                return http_response
        else:
            result = "Invalid login"
            payload = result
            http_response = HttpResponse(payload)
            http_response['Content-Type'] = 'text/plain'
            http_response.status_code = 401
            http_response.reason_phrase = "Unauthorized Request"
            return http_response
    else:
        result = "Bad Request"
        payload = result
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response.status_code = 400
        http_response.reason_phrase = "Bad Request"
        return http_response


@csrf_exempt
def HandleLogout(request):
    if request.method == 'POST':
        logout(request)
        print("logged out")
        result = "logged out"
        payload = result
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response.status_code = 200
        http_response.reason_phrase = "OK"
        return http_response


@csrf_exempt
def HandleDeleteStory(request):
    user = auth.get_user(request)
    if request.method == 'POST':

        if user.is_authenticated:
            jsonContent = json.loads(request.body)
            delete_key = jsonContent['story_key']
            result = Story.objects.get(key=delete_key)
            result.delete()

            result = "Deleted Story"
            payload = result
            http_response = HttpResponse(payload)
            http_response['Content-Type'] = 'text/plain'
            http_response.status_code = 201
            http_response.reason_phrase = "CREATED"
            return http_response
        else:
            result = "Bad Request"
            payload = result
            http_response = HttpResponse(payload)
            http_response['Content-Type'] = 'text/plain'
            http_response.status_code = 503
            http_response.reason_phrase = "Service Unavailable"
            return http_response


@csrf_exempt
def HandleGetStories(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if request.method != 'GET':
        http_bad_response.content = 'Only GET requests are allowed for this resource\n'
        return http_bad_response

    elif request.method == 'GET':
        jsonContent = json.loads(request.body)
        category = jsonContent['story_cat']
        region = jsonContent['story_region']
        story_date = jsonContent['story_date']
        global results

        if category == '*':
            category = '.*'
        if region == '*':
            region = '.*'

        if story_date != '*':
            story_date = datetime.strptime(story_date, '%d/%m/%Y')
            results = Story.objects.filter(category__iregex=category, region__iregex=region,
                                           date=story_date.strftime('%Y-%m-%d'))
        elif story_date == '*':
            results = Story.objects.filter(category__iregex=category, region__iregex=region)

        if (len(results)) == 0:
            result = "No stories found"
            payload = result
            http_response = HttpResponse(payload)
            http_response['Content-Type'] = 'text/plain'
            http_response.status_code = 404
            http_response.reason_phrase = "Not Found"
            return http_response

        stories = []
        for record in results:
            item = {'key': record.id, 'headline': record.headline, 'story_cat': record.category,
                    'story_region': record.region, 'author': record.author.authorName,
                    'story_details': record.details, 'story_date': str(record.date)}
            stories.append(item)

        payload = json.dumps({'stories': stories})
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'application/json'
        http_response.status_code = 200
        http_response.reason_phrase = "OK"
        return http_response

    else:
        result = "Bad Request"
        payload = json.dumps({'result': result})
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response.status_code = 503
        http_response.reason_phrase = "Service Unavailable"
        return http_response


@csrf_exempt
def HandlePostStory(request):
    user = auth.get_user(request)

    if user.is_authenticated:

        jsonContent = json.loads(request.body)
        headline = jsonContent['headline']
        category = jsonContent['category']
        region = jsonContent['region']
        details = jsonContent['details']

        username = user.author_set.all()[0]
        date = datetime.now()

        s1 = Story(headline=headline, category=category, region=region, details=details,
                   author=username, date=date)
        s1.save()

        result = "Story Added"
        payload = result
        http_response = HttpResponse(payload)
        http_response.status_code = 201
        http_response.reason_phrase = "CREATED"
        return http_response
    else:
        result = "Service Unavailable"
        payload = result
        http_response = HttpResponse(payload)
        http_response['Content-Type'] = 'text/plain'
        http_response.status_code = 503
        http_response.reason_phrase = "Service Unavailable"
        return http_response
