
from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Author(models.Model):
    authorName = models.CharField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # removed because field holds plaintext
    # authorPassword = models.CharField(max_length=64)

    def __str__(self):
        return self.user.username


class Story(models.Model):
    headline = models.CharField(max_length=64)
    storyTypes = [('pol', 'Politics'), ('art', 'Art news'), ('tech', 'Technology News'), ('trivia', 'Trivial News')]
    category = models.CharField(max_length=32, choices=storyTypes, default='unknown')
    storyRegions = [('uk', 'United Kingdom'), ('eu', 'European news'), ('w', 'World news')]
    region = models.CharField(max_length=32, choices=storyRegions, default='unknown')
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField()
    details = models.CharField(max_length=512)
