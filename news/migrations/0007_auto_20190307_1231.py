# Generated by Django 2.1.7 on 2019-03-07 12:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_auto_20190307_1147'),
    ]

    operations = [
        migrations.RenameField(
            model_name='author',
            old_name='Authorusername',
            new_name='authorUsername',
        ),
    ]
