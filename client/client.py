import requests
import json

client = requests.session()

URL = ''


def login(username, password):
    r = client.post(URL + '/api/login/',
                    data={'username': username, 'password': password})

    if r.status_code == 200:
        try:
            print(r.text, '\n')
        except:
            print("issue with logging in")

    elif r.status_code == 403:
        try:
            print(r.text, '\n')
        except:
            print("issue with Disabled Account")

    elif r.status_code == 401:
        try:
            print(r.text, '\n')
        except:
            print("issue with Unauthorized Request")

    elif r.status_code == 400:
        try:
            print(r.text, '\n')
        except:
            print("issue with Bad Request")


def logout():
    r = client.post(URL + '/api/logout/')

    if r.status_code == 200:
        try:
            print(r.text, '\n')
        except:
            print("issue with logging out")


def delete(story_key):
    payload = ({'story_key': story_key})
    r = client.post(URL + '/api/deletestory/', json=payload)

    if r.status_code == 201:
        try:
            print(r.text, '\n')
        except:
            print("issue with Delete Story")

    elif r.status_code == 503:
        try:
            print(r.text, '\n')
        except:
            print("issue with Bad Request")


def getStories(news_id, category, region, date):
    payload = ({'story_cat': category, 'story_region': region, 'story_date': date})

    agencies = client.get('http://directory.pythonanywhere.com/api/list')

    entry = json.loads(agencies.text)

    if news_id == "*":
        for agency in entry['agency_list']:

            r = client.get(agency['url'] + '/api/getstories/', json=payload)

            if r.status_code == 200:

                try:
                    content = json.loads(r.text)
                    for story in content['stories']:
                        print('Story:', story['key'])
                        print('Headline:', story['headline'])
                        print('Category:', story['story_cat'])
                        print('Region:', story['story_region'])
                        print('Author:', story['author'])
                        print('Date:', story['story_date'])
                        print('Details:', story['story_details'], '\n')
                except:
                    print("issue retrieving news stories")

            elif r.status_code == 503:
                try:
                    print(r.text, '\n')
                except:
                    print("issue with Service Unavailable")

            elif r.status_code == 404:
                try:
                    print(r.text, '\n')
                except:
                    print("Failing with no stories")


    else:
        for agency in entry['agency_list']:

            if agency['agency_code'] == news_id:

                r = client.get(agency['url'] + '/api/getstories/', json=payload)

                if r.status_code == 200:

                    try:
                        content = json.loads(r.text)
                        for story in content['stories']:
                            print('Story:', story['key'])
                            print('Headline:', story['headline'])
                            print('Category:', story['story_cat'])
                            print('Region:', story['story_region'])
                            print('Author:', story['author'])
                            print('Date:', story['story_date'])
                            print('Details:', story['story_details'], '\n')
                    except:
                        print("issue retrieving news stories")

                elif r.status_code == 503:
                    try:
                        print(r.text, '\n')
                    except:
                        print("issue with Service Unavailable")


                elif r.status_code == 404:
                    try:
                        print(r.text, '\n')
                    except:
                        print("Failing with no stories")


def postStory(headline, category, region, details):
    payload = ({'headline': headline, 'category': category, 'region': region, 'details': details})
    r = client.post(URL + '/api/poststory/', json=payload)

    if r.status_code == 201:
        try:
            print(r.text, '\n')
        except:
            print("issue with posting story")

    elif r.status_code == 503:
        try:
            print(r.text, '\n')
        except:
            print("issue with Service Unavailable")


def main():
    while True:
        userinput = input("Enter a command \n")
        usrinput = userinput.split()

        if usrinput[0] == 'exit':
            break

        elif usrinput[0] == 'login':
            global URL
            if (len(usrinput)) > 1:

                if not usrinput[1].__contains__('http'):
                    usrinput[1] = 'http://'.__add__(usrinput[1])

                if URL == '':

                    username = input("username: ").strip()
                    password = input("password: ").strip()

                    URL = usrinput[1]
                    r = client.post(URL + '/api/login/',
                                    data={'username': username, 'password': password})

                    if r.status_code is not 200:
                        URL = ''

                    print(r.text, '\n')

                else:
                    print("Logout from previous session")
            else:
                print("Invalid command usage")

        elif usrinput[0] == "logout":

            if URL == '':
                print("No service logged into")
            else:
                r = client.post(URL + '/api/logout/')

                if r.status_code == 200:
                    URL = ''
                    try:
                        print(r.text, '\n')
                    except:
                        print("issue with logging out")


        elif usrinput[0] == "post":

            headline = input("enter headline\n")
            category = input("enter category\n")
            region = input("enter region\n")
            details = input("enter details\n")
            postStory(headline, category, region, details)

        elif usrinput[0] == "delete":

            if (len(usrinput)) > 1:
                story_key = usrinput[1]
                delete(story_key)
            else:
                print("2 values minimum... delete key\n")

        elif usrinput[0] == "news":

            if len(usrinput) > 1:
                news_id = '*'
                category = '*'
                region = '*'
                date = '*'

                for string in usrinput:
                    if string.__contains__('id'):
                        news_id = string.replace("-id=", "")
                    if string.__contains__('cat'):
                        category = string.replace("-cat=", "")
                    if string.__contains__('reg'):
                        region = string.replace("-date=", "")
                    if string.__contains__('date'):
                        date = string.replace("-date=", "")

                getStories(news_id, category, region, date)
            else:
                news_id = '*'
                category = '*'
                region = '*'
                date = '*'
                getStories(news_id, category, region, date)


if __name__ == "__main__":
    main()
